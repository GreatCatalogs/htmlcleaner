package com.catalogs.html.cleaner;


import com.catalogs.html.cleaner.config.ApplicationProps;
import com.catalogs.html.cleaner.context.Context;
import com.catalogs.html.cleaner.executor.HtmlCleanerExecutor;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;

@Component
public class Main {

    private static Logger _logger = Logger.getLogger(Main.class);

    private static String[] configLocations = new String[]{
        "classpath:/spring/spring-common.xml"
    };

    @Autowired
    private ApplicationProps appProps;
    
    @Autowired
    private HtmlCleanerExecutor htmlCleanerExecutor;
    
    public static void main(String[] args) {
        _logger.info("Running Main class.");
        ApplicationContext context = new ClassPathXmlApplicationContext(configLocations);
        Main mainBean = context.getBean(Main.class);
        mainBean.executeHtmlCleaner(args, context);  
        _logger.info("End Running Main class.");
    }

    private void executeHtmlCleaner(String[] args, ApplicationContext ctx) {
        _logger.info("executeHtmlCleaner start...");

        _logger.info("Reading config params from args[]...");
        String inputDir = args[0];
        String outputDir = args[1];
        String fileName = args[2];

        _logger.info("Input Directory: " + inputDir);
        _logger.info("Output Directory: " + outputDir);
        _logger.info("File Name: " + fileName);

        Context context = new Context(inputDir, outputDir, fileName);
        htmlCleanerExecutor.executeHtmlCleaner(context);
        
        _logger.info("executeHtmlCleaner end...");
    }
    
}
