package com.catalogs.html.cleaner.processor;

import com.catalogs.html.cleaner.context.Context;

import java.io.File;
import java.io.IOException;

public interface FileProcessor {
    
    void processFile(File file, Context context);
    
}
