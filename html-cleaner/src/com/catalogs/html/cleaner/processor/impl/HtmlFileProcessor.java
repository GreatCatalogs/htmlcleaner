package com.catalogs.html.cleaner.processor.impl;

import com.catalogs.html.cleaner.context.Context;
import com.catalogs.html.cleaner.mail.MailSender;
import com.catalogs.html.cleaner.model.Line;
import com.catalogs.html.cleaner.processor.FileProcessor;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class HtmlFileProcessor implements FileProcessor {

    private static Logger _logger = Logger.getLogger(HtmlFileProcessor.class);

    @Autowired
    private MailSender mailSender;

    @Override
    public void processFile(File file, Context context) {

        String fileName = file.getName();
        _logger.info("processFile - start processing file: '" + fileName + "'");

        String outputDir = context.getOutputDir();

        String line = "";
        String delimiter = "\",\"";
        BufferedReader br = null;
        BufferedWriter passedWriter = null;
        BufferedWriter failedWriter = null;
        try {

            String baseFileName = StringUtils.substringBeforeLast(fileName, ".");
            String extension = StringUtils.substringAfterLast(fileName, ".");
            String goodFileName = outputDir + "/" + baseFileName + "_passed." + extension;
            String badFileName = outputDir + "/" + baseFileName + "_failed." + extension;
            passedWriter = new BufferedWriter(new FileWriter(new File(goodFileName)));
            failedWriter = new BufferedWriter(new FileWriter(new File(badFileName)));

            br = new BufferedReader(new FileReader(file));

            String headerLine = br.readLine();
            passedWriter.write(headerLine + "\r\n");
            failedWriter.write(headerLine + "\r\n");

            int lineNumber = 1;
            List<Line> errorLines = new ArrayList<Line>();

            while ((line = br.readLine()) != null) {

                lineNumber++;

                boolean lineValid = true;

                line = StringUtils.removeStart(line, "\"");
                line = StringUtils.removeEnd(line, "\"");

                String[] lineItems = line.split(delimiter);
                for (String lineItem : lineItems) {

                    boolean hasExtraQuotes = lineItem.contains("\"");
                    if (hasExtraQuotes) {
                        //HTML is not valid if can not be parsed

                        //add line to errorLines
                        String lineStart = "\"" + StringUtils.substring(line, 0, 100) + "...";
                        Line errorLine = new Line(lineNumber, lineStart, "Double quotes detected inside CSV item value.");
                        errorLines.add(errorLine);

                        _logger.info("Invalid line #" + lineNumber + ": " + lineStart + ". Error: " + "Double quotes detected inside CSV item value.");

                        lineValid = false;
                        break;
                    }


                    boolean isHtml = lineItem.contains("<") || lineItem.contains(">");

                    if (!isHtml) {
                        continue;
                    }

                    try {
                        DocumentBuilder parser = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                        String preparedLine = prepareLineForParsing(lineItem);
                        parser.parse(new InputSource(new StringReader(preparedLine)));
                    } catch (Exception e) {
                        //HTML is not valid if can not be parsed

                        //add line to errorLines
                        String lineStart = "\"" + StringUtils.substring(line, 0, 100) + "...";
                        Line errorLine = new Line(lineNumber, lineStart, e.getMessage());
                        errorLines.add(errorLine);

                        _logger.info("Invalid line #" + lineNumber + ": " + lineStart + ". Error: " + e.getMessage());

                        lineValid = false;
                        break;
                    }
                }

                String lineToSave = prepareLineForSaving(line);
                if (lineValid) {
                    passedWriter.write(lineToSave);
                } else {
                    failedWriter.write(lineToSave);
                }
            }

            int errorLineCount = errorLines.size();
            if (errorLineCount > 0) {

                String subject = "Invalid lines in CSV File: " + fileName;

                Map<String, Object> data = new HashMap<String, Object>();
                data.put("fileName", fileName);
                data.put("error_lines", errorLines);
                data.put("lineCount", errorLineCount);

                mailSender.sendEmail(subject, "vm/error-email.vm", data, context);
            }


            _logger.info("Invalid lines count: " + errorLineCount);

        } catch (Exception e) {
            _logger.error("Error: " + e.getMessage());
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (Exception ignored) {
                }
            }
            if (passedWriter != null) {
                try {
                    passedWriter.close();
                } catch (Exception ignored) {
                }
            }
            if (failedWriter != null) {
                try {
                    failedWriter.close();
                } catch (Exception ignored) {
                }
            }
        }

        _logger.info("processFile - end processing file: '" + fileName + "'");
    }

    private String prepareLineForParsing(String line) {
        //Wrap with tag to make it valid HTML
        return "<html>" + prepareLine(line) + "</html>";
    }

    private String prepareLineForSaving(String line) {
        //Wrap with quotes and add new line
        return "\"" + prepareLine(line) + "\"\r\n";
    }


    private String prepareLine(String line) {
        //Suppress <br> tags
        String preparedLine = line.replace("<br>", "");
        preparedLine = preparedLine.replace("<br/>", "");
        preparedLine = preparedLine.replace("<BR>", "");
        preparedLine = preparedLine.replace("<BR/>", "");

        //Replace special symbols
        preparedLine = preparedLine.replace("&", "&amp;");
        preparedLine = preparedLine.replace("%", "&#37;");

        //Suppress <i> tags
        preparedLine = preparedLine.replace("<i>", "");
        preparedLine = preparedLine.replace("</i>", "");
        preparedLine = preparedLine.replace("<I>", "");
        preparedLine = preparedLine.replace("</I>", "");

        //Suppress <b> tags
        preparedLine = preparedLine.replace("<b>", "");
        preparedLine = preparedLine.replace("</b>", "");
        preparedLine = preparedLine.replace("<B>", "");
        preparedLine = preparedLine.replace("</B>", "");

        //Covert <ul>/<li> tags to lowercase
        preparedLine = preparedLine.replace("<UL>", "<ul>");
        preparedLine = preparedLine.replace("</UL>", "</ul>");
        preparedLine = preparedLine.replace("<LI>", "<li>");
        preparedLine = preparedLine.replace("</li>", "</li>");

        //fix for invalid href
        preparedLine = preparedLine.replace("font face=\" arial,sans-serif;", "");

        //Return prepared line
        return preparedLine;
    }

}
