package com.catalogs.html.cleaner.mail.impl;

import com.catalogs.html.cleaner.context.Context;
import com.catalogs.html.cleaner.mail.GMailAuthenticator;
import com.catalogs.html.cleaner.mail.MailSender;
import com.catalogs.html.cleaner.mail.util.MailConfigUtil;
import com.catalogs.html.cleaner.model.MailConfig;
import org.apache.log4j.Logger;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.springframework.stereotype.Service;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.StringWriter;
import java.util.Map;
import java.util.Properties;

@Service
public class CatalogsMailSender extends AbstractMailSender implements MailSender {

    private static Logger _logger = Logger.getLogger(CatalogsMailSender.class);

    @Override
    public void sendEmail(String subject, String templateName, Map data, Context context) {

        _logger.info("Sending email...");

        Properties props = new Properties();

        try {

            String mailConfigFilePath = context.getInputDir() + "/mail.properties";
            /*String mailConfigFilePath = appProps.getMailConfigFilePath();*/
            
            MailConfig mailConfig = MailConfigUtil.getMailConfig(mailConfigFilePath);

            String smtpUser = mailConfig.getUser();
            String smtpPassword = mailConfig.getPassword();
            String smtpHost = mailConfig.getHost();
            String smtpPort = mailConfig.getPort();

            String[] emailsTo = mailConfig.getEmailsTo();

            props.setProperty("mail.smtp.host", smtpHost);
            props.setProperty("mail.smtp.port", smtpPort);
            props.setProperty("mail.smtp.auth", "true");
            props.setProperty("mail.smtp.starttls.enable", "true");
            props.setProperty("mail.smtp.user", smtpUser);
            props.setProperty("mail.smtp.password", smtpPassword);

            Session session = Session.getInstance(props, new GMailAuthenticator(smtpUser, smtpPassword));

            // Create a default MimeMessage object.
            MimeMessage message = new MimeMessage(session);

            // Set From: header field of the header.
            message.setFrom(new InternetAddress(smtpUser));

            // Set To: header field of the header.
            for (String emailTo : emailsTo) {
                message.addRecipient(Message.RecipientType.TO, new InternetAddress(emailTo));
            }

            // Set Subject: header field
            message.setSubject(subject);

            Template template = ve.getTemplate(templateName);
            VelocityContext vc = new VelocityContext(data);

            StringWriter writer = new StringWriter();
            template.merge(vc, writer);
            String content = writer.toString();

            // Now set the actual message
            message.setContent(content, "text/html");
            message.saveChanges();

            // Send message
            Transport transport = session.getTransport("smtp");
            transport.connect(smtpHost, smtpUser, smtpPassword);
            Transport.send(message);
            transport.close();

        } catch (Exception e) {
            _logger.error("Can not send email: ", e);
        }

        _logger.info("Email has been sent successfully.");
        
    }
}
