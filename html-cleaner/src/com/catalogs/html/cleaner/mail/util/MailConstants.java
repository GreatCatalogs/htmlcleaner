package com.catalogs.html.cleaner.mail.util;

public interface MailConstants {
    
    String SMTP_USER = "smtp.user";
    String SMTP_PASSWORD = "smtp.password";
    String SMTP_HOST = "smtp.host";
    String SMTP_PORT = "smtp.port";
    
    String SMTP_SEND_TO = "email.send.to";
    
}
