package com.catalogs.html.cleaner.mail.util;

import com.catalogs.html.cleaner.model.MailConfig;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.io.FileInputStream;
import java.util.Properties;

public class MailConfigUtil {

    private static Logger _logger = Logger.getLogger(MailConfigUtil.class);
    
    public static MailConfig getMailConfig(String mailConfigFile) throws Exception {

        _logger.info("MailConfigUtil: creating mail config.");

        MailConfig mailConfig = new MailConfig();
        
        try {

            Properties properties = new Properties();

            _logger.info("Loading properties from config file: " + mailConfigFile);
            FileInputStream fileInputStream = new FileInputStream(mailConfigFile);
            properties.load(fileInputStream);
            fileInputStream.close();
            _logger.info("Properties loaded successfully.");

            String user = properties.getProperty(MailConstants.SMTP_USER);
            String password = properties.getProperty(MailConstants.SMTP_PASSWORD);
            String host = properties.getProperty(MailConstants.SMTP_HOST);
            String port = properties.getProperty(MailConstants.SMTP_PORT);

            String sendToString = properties.getProperty(MailConstants.SMTP_SEND_TO);
            String[] emailsTo = StringUtils.split(sendToString, ",");

            _logger.info("MailConfigUtil: mail config created successfully");
            mailConfig = new MailConfig(user, password, host, port, emailsTo);

        } catch (Exception e) {
            _logger.error("Error: " + e.getMessage());
            e.printStackTrace();
        }
        
        return mailConfig;
    }
    
}
