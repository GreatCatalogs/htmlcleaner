package com.catalogs.html.cleaner.mail;

import com.catalogs.html.cleaner.context.Context;

import java.util.Map;

public interface MailSender {

    void sendEmail(String subject, String templateName, Map data, Context context);

}
