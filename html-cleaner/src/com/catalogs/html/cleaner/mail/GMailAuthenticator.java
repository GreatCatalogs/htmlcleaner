package com.catalogs.html.cleaner.mail;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

public class GMailAuthenticator extends Authenticator {

    private String user;
    private String password;

    public GMailAuthenticator(String username, String password) {
        super();
        this.user = username;
        this.password = password;
    }

    public PasswordAuthentication getPasswordAuthentication() {
        return new PasswordAuthentication(user, password);
    }
}
