package com.catalogs.html.cleaner.scanner;

import java.io.File;
import java.util.Collection;

public interface FileScanner {

    Collection<File> readFiles(String scanDirectory);
    
}
