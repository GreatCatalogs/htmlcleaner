package com.catalogs.html.cleaner.scanner.impl;

import com.catalogs.html.cleaner.scanner.FileScanner;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.CanReadFileFilter;
import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.Collection;

@Service
public class FileScannerImpl implements FileScanner {

    private static Logger _logger = Logger.getLogger(FileScannerImpl.class);

    @SuppressWarnings("unchecked")
    @Override
    public Collection<File> readFiles(String scanDirectory) {

        _logger.info("com.catalogs.html.cleaner.scanner.impl.FileScannerImpl.readFiles start...");

        File scanDir = new File(scanDirectory);

        Collection files = FileUtils.listFiles(scanDir, CanReadFileFilter.CAN_READ, DirectoryFileFilter.INSTANCE);

        _logger.info("com.catalogs.html.cleaner.scanner.impl.FileScannerImpl.readFiles end...");
        
        return files;
    }
}
