package com.catalogs.html.cleaner.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class ApplicationProps {

    @Value("${input.file.dir}")
    private String inputFileDir;

    @Value("${output.file.dir}")
    private String outputFileDir;

/*    @Value("${mail.config.file.path}")
    private String mailConfigFilePath;*/

    public String getInputFileDir() {
        return inputFileDir;
    }

    public void setInputFileDir(String inputFileDir) {
        this.inputFileDir = inputFileDir;
    }

    public String getOutputFileDir() {
        return outputFileDir;
    }

    public void setOutputFileDir(String outputFileDir) {
        this.outputFileDir = outputFileDir;
    }

   /* public String getMailConfigFilePath() {
        return mailConfigFilePath;
    }

    public void setMailConfigFilePath(String mailConfigFilePath) {
        this.mailConfigFilePath = mailConfigFilePath;
    }*/
}
