package com.catalogs.html.cleaner.model;

public class MailConfig {

    private String user;
    private String password;
    private String host;
    private String port;
    private String[] emailsTo;

    public MailConfig() {
    }

    public MailConfig(String user, String password, String host, String port, String[] emailsTo) {
        this.user = user;
        this.password = password;
        this.host = host;
        this.port = port;
        this.emailsTo = emailsTo;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String[] getEmailsTo() {
        return emailsTo;
    }

    public void setEmailsTo(String[] emailsTo) {
        this.emailsTo = emailsTo;
    }
}
