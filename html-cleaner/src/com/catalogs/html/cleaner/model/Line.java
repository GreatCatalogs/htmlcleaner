package com.catalogs.html.cleaner.model;

public class Line {
    
    private int number;
    private String content;
    private String errorMsg;

    public Line() {
    }

    public Line(int number, String content, String errorMsg) {
        this.number = number;
        this.content = content;
        this.errorMsg = errorMsg;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }
}
