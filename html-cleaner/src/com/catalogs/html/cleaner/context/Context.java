package com.catalogs.html.cleaner.context;

public class Context {

    private String inputDir;
    private String outputDir;
    private String fileName;

    public Context() {
    }

    public Context(String inputDir, String outputDir, String fileName) {
        this.inputDir = inputDir;
        this.outputDir = outputDir;
        this.fileName = fileName;
    }

    public String getInputDir() {
        return inputDir;
    }

    public void setInputDir(String inputDir) {
        this.inputDir = inputDir;
    }

    public String getOutputDir() {
        return outputDir;
    }

    public void setOutputDir(String outputDir) {
        this.outputDir = outputDir;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
