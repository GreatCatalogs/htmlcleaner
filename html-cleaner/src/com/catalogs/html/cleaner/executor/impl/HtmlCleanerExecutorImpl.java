package com.catalogs.html.cleaner.executor.impl;

import com.catalogs.html.cleaner.context.Context;
import com.catalogs.html.cleaner.executor.HtmlCleanerExecutor;
import com.catalogs.html.cleaner.processor.FileProcessor;
import com.catalogs.html.cleaner.scanner.FileScanner;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;

@Service
public class HtmlCleanerExecutorImpl implements HtmlCleanerExecutor {

    private static Logger _logger = Logger.getLogger(HtmlCleanerExecutorImpl.class);
    
    @Autowired
    private FileScanner fileScanner;
    
    @Autowired
    private FileProcessor fileProcessor;
    
    @Override
    public void executeHtmlCleaner(Context context) {
        _logger.info("executeHtmlCleaner start...");

        try {
            /*Collection<File> files = fileScanner.readFiles(context.getInputDir());
            Iterator<File> iterator = files.iterator();*/

            String fileName = context.getFileName();
            String inputDir = context.getInputDir();

            File file = new File(inputDir + "/" + fileName);
            
            fileProcessor.processFile(file, context);

            /*while (iterator.hasNext()) {

                File file = iterator.next();

                fileProcessor.processFile(file, context);
            }*/

        } catch (Exception e) {
            _logger.error("Failed to execute HTML cleaner: " + e.getMessage());
        }
        
        _logger.info("executeHtmlCleaner end...");
    }
    
    
}
