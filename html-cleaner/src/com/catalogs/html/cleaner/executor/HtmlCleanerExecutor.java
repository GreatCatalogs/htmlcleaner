package com.catalogs.html.cleaner.executor;

import com.catalogs.html.cleaner.context.Context;

public interface HtmlCleanerExecutor {
    
    void executeHtmlCleaner(Context context);
    
}
