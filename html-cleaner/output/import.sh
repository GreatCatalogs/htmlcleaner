#!/bin/bash
# POSTING FEEDS TO SOLR Search Engine.

#Clean up first

rm  solr_search_feed.csv  search_feed_passed.csv solr_search_feed_failed.csv

# Clear the search Engine
#curl http://images.catalogs.com:8080/solr/catalogsdev/update -H "Content-Type: text/xml" --data-binary '<delete><query>*:*</query></delete>'
# Download the Feed
wget   --output-document solr_search_feed.csv  http://www.catalogs.com/solr_search/search_feed_export_bad.asp


./html_cleaner.sh solr_search_feed.csv

# Upload and process

curl http://images.catalogs.com:8080/solr/catalogsdev/update/csv --data-binary @solr_search_feed_passed.csv -H 'Content-type:text/csv; charset=utf-8'

#curl http://images.catalogs.com:8080/solr/catalogsdev/update -H "Content-Type: text/xml" --data-binary '<commit/>'
#curl http://images.catalogs.com:8080/solr/catalogsdev/update -H "Content-Type: text/xml" --data-binary '<optimize/>'

#curl http://images.catalogs.com:8080/solr/prodcat/update -H "Content-Type: text/xml" --data-binary '<commit/>'
#curl http://images.catalogs.com:8080/solr/prodcat/update -H "Content-Type: text/xml" --data-binary '<optimize/>'


