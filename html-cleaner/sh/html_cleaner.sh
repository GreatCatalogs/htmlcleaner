#!/bin/bash

process_command()
{
  if [ $? -eq 0 ]
  then
    echo $1
  else
    echo $2
    exit 1
  fi
}

#Read file name from console
#read -p "Enter File Name: " fileName
#fileName=${fileName:-search_feed_export.csv}
#echo $fileName

inputDir=.
outputDir=.

echo Executing HTML Cleaner
java -jar html_cleaner.jar $inputDir $outputDir $1
process_command 'HTML Cleaner finished successfully.' 'Fail: HTML Cleaner failed.'