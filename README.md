#HTML Cleaner Tool [](id=html cleaner)

##Purpose

This tool processes CSV files with HTML content and performs HTML validation.
It scans all the files in input directory, and for each file [FILE.asp] creates two files in output directory:

- [FILE_passed.asp] - with valid records only;

- [FILE_failed.asp] - with invalid records only.

##Distribution

This tool is distributed in a form of shell script which runs Java jar file, see:
`html-cleaner/output/html_cleaner.sh`, `html-cleaner/output/html_cleaner.jar`

##Configuration

To configure this tool you need to change *inputDir* (1) and *outputDir* (2) parameters in `html-cleaner/output/html_cleaner.sh` file:
![Configuration](html-cleaner/images/sh-config.jpg)

##Running

To run this tool you need:

- Download `html-cleaner/output/html_cleaner.sh` and `html-cleaner/output/html_cleaner.jar` files.

- Configure *inputDir* and *outputDir* parameters in *html_cleaner.sh* file.

- Place CSV files which you need to process into 'inputDir' (which you specified in configuration).

- Navigate to folder with downloaded files in console (1), make html_cleaner.sh file executable (2) and run it (3):
![Running](html-cleaner/images/running-sh.jpg)

- Appropriate files will be created in 'outputDir' directory. You can also see actual HTML errors, which cause validation failure:
![Html Errors](html-cleaner/images/html-errors.jpg)